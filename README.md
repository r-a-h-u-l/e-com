# <h1 align="center">Api's For E-Commerce website</h1>

<!-- ABOUT THE PROJECT -->
## About The Project

This Project contain all backend work for e-commerce website. This project only contain api's for user, category, product,order, payment. we're using [braintree](https://developers.braintreepayments.com/) for payment.
 
### Built With
 
* [Python](https://www.python.org/)
* [Django](https://www.djangoproject.com/)
* [Django Rest Framework](https://www.django-rest-framework.org/)
 
### Installation
 
1. Download or clone the Repository to your device
2. Create Virtual Environment using `python3 -m venv <your environment name>`
3. Activate Virtual Environment for Mac and Linux user `source <virtual env path>/bin/activate`
4. Activate Virtual Environment for Windows user `venv\Scripts\activate`
5. type `pip install -r requirements.txt` (this will install required package for project)
6. type `python3 manage.py makemigrations`
7. type `python3 manage.py migrate`
8. you can check all api call using [POSTMAN](https://www.postman.com/)
